package patchara.projectgroup;

import patchara.projectgroup.model.User;
import patchara.projectgroup.service.UserService;

public class TestUserService {

    public static void main(String[] args) {
        UserService service = new UserService();
        User user = service.login("admin", "pass");
        System.out.println(user);
    }
}
