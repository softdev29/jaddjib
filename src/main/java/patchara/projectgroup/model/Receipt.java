/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import patchara.projectgroup.dao.CustomerDao;
import patchara.projectgroup.dao.EmployeeDao;
import patchara.projectgroup.dao.StoreDao;

/**
 *
 * @author Pla
 */
public class Receipt {

    private int id;
    Store store;
    Employee employee;
    Customer customer;
    private int qty;
    private double total;
    private double discount;
    private double netTotal;
    private double cash;
    private double change;
    private ArrayList<ReceiptItem> receiptItem;
    private Date date;

    public Receipt(int id, Store store, Employee employee, Customer costomer, int qty, double total, double discount, double netTotal, double cash, double change, ArrayList<ReceiptItem> receiptItem, Date date) {
        this.id = id;
        this.store = store;
        this.employee = employee;
        this.customer = costomer;
        this.qty = qty;
        this.total = total;
        this.discount = discount;
        this.netTotal = netTotal;
        this.cash = cash;
        this.change = change;
        this.receiptItem = receiptItem;
        this.date = date;
    }

    public Receipt(Store store, Employee employee, Customer costomer, int qty, double total, double discount, double netTotal, double cash, double change, Date date) {
        this.id = -1;
        receiptItem = new ArrayList<>();
        this.store = store;
        this.employee = employee;
        this.customer = costomer;
        this.qty = qty;
        this.total = total;
        this.discount = discount;
        this.netTotal = netTotal;
        this.cash = cash;
        this.change = change;
        this.date = date;
    }

    public Receipt(double discount, double cash, double change) {
        this.customer = null;
        receiptItem = new ArrayList<>();
        this.qty = 0;
        this.total = total;
        this.discount = discount;
        this.cash = cash;
        this.change = change;
        date = new Date();
    }

    public Receipt() {
        this.customer = null;
        receiptItem = new ArrayList<>();
        this.qty = 0;
        this.total = 0;
        date = new Date();
    }
    
    
    public Receipt(int id) {
        this.id=id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }
    public void setStoreId(int id){
        this.store.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    public void setEmployeeId(int id){
        this.employee.id = id ;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer costomer) {
        this.customer = costomer;
    }
    public void setCustomerId(int id) {
        this.customer.id = id ;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getNetTotal() {
        return total - discount;
    }

    public void setNetTotal(double netTotal) {
        this.netTotal = netTotal;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double change) {
        this.change = change;
    }

    public ArrayList<ReceiptItem> getReceiptItem() {
        return receiptItem;
    }

    public void setReceiptItem(ArrayList<ReceiptItem> receiptItem) {
        this.receiptItem = receiptItem;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void addOrderDetail(ReceiptItem item) {
        receiptItem.add(item);
        qty = qty + item.getQty();
        total = total + item.getTotal();
    }

    public void addOrderDetail(Product product, int qty) {
        ReceiptItem item = new ReceiptItem(product, product.getName(), qty, product.getPrice(), product.getPrice() * qty, this);
        this.addOrderDetail(item);
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", store_id=" + store.getId() + ", employee_id=" + employee + ", costomer_id=" + customer + ", qty=" + qty + ", total=" + total + ", discount=" + discount + ", netTotal=" + netTotal + ", cash=" + cash + ", change=" + change + ", receiptItem=" + receiptItem + ", date=" + date + '}';
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        StoreDao storeDao = new StoreDao();
        EmployeeDao employeeDao = new EmployeeDao();
        CustomerDao customerDao = new CustomerDao();
        try {
            receipt.setId(rs.getInt("receipt_id"));
            int storeid = rs.getInt("store_id");
            Store store = storeDao.get(storeid);
            receipt.setStore(store);

            int employeeid = rs.getInt("employee_id");
            Employee emp = employeeDao.get(employeeid);
            receipt.setEmployee(emp);

            int customerid = rs.getInt("customer_id");
            Customer cus = customerDao.get(customerid);
            receipt.setCustomer(cus);
            
            receipt.setQty(rs.getInt("qty"));
            receipt.setTotal(rs.getDouble("total"));
            receipt.setDiscount(rs.getDouble("discount"));
            receipt.setNetTotal(rs.getDouble("net_total"));
            receipt.setCash(rs.getDouble("cash"));
            receipt.setChange(rs.getDouble("change"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            receipt.setDate(sdf.parse(rs.getString("date")));

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
    }
}
