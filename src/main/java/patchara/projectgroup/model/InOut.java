/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import patchara.projectgroup.dao.EmployeeDao;

/**
 *
 * @author user1
 */
public class InOut {

    int id;
    Employee employee;
    Date date;
    String startTime;
    String endTime;
    String period;

    public InOut(int id, Employee employee, Date date, String start_time, String end_time, String period) {
        this.id = id;
        this.employee = employee;
        this.date = date;
        this.startTime = start_time;
        this.endTime = end_time;
        this.period = period;
    }

    public InOut() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String start_time) {
        this.startTime = start_time;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String end_time) {
        this.endTime = end_time;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    @Override
    public String toString() {
        return "InOut{" + "id=" + id + ", employee=" + employee + ", date=" + date + ", start_time=" + startTime + ", end_time=" + endTime + ", period=" + period + '}';
    }

    public static InOut fromRS(ResultSet rs) {
        InOut io = new InOut();
        EmployeeDao employeeDao = new EmployeeDao();
        try {
            io.setId(rs.getInt("io_id"));
            
            int employeeid = rs.getInt("employee_id");
            Employee emp = employeeDao.get(employeeid);
            io.setEmployee(emp);
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            io.setDate(sdf.parse(rs.getString("io_date")));
            
            io.setStartTime(rs.getString("io_start_time"));
            io.setEndTime(rs.getString("io_end_time"));
            io.setPeriod(rs.getString("io_period"));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        return io;
    }
}
