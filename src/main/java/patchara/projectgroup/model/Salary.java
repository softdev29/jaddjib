/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import patchara.projectgroup.dao.EmployeeDao;

/**
 *
 * @author user1
 */
public class Salary {

    int id;
    Employee employee;
    Date payDay;
    int otUnit;
    int otTotal;
    int salary;
    int Total;

    public Salary(int id, Employee employee, Date payDay, int otUnit, int otTotal, int salary, int Total) {
        this.id = id;
        this.employee = employee;
        this.payDay = payDay;
        this.otUnit = otUnit;
        this.otTotal = otTotal;
        this.salary = salary;
        this.Total = Total;
    }

    public Salary() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getPayDay() {
        return payDay;
    }

    public void setPayDay(Date payDay) {
        this.payDay = payDay;
    }

    public int getOtUnit() {
        return otUnit;
    }

    public void setOtUnit(int otUnit) {
        this.otUnit = otUnit;
    }

    public int getOtTotal() {
        return otTotal;
    }

    public void setOtTotal(int otTotal) {
        this.otTotal = otTotal;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getTotal() {
        return Total;
    }

    public void setTotal(int Total) {
        this.Total = Total;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", employee=" + employee + ", payDay=" + payDay + ", otUnit=" + otUnit + ", otTotal=" + otTotal + ", salary=" + salary + ", Total=" + Total + '}';
    }

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        EmployeeDao employeeDao = new EmployeeDao();
        try {
            salary.setId(rs.getInt("salary_id"));
            int employeeid = rs.getInt("employee_id");
            Employee emp = employeeDao.get(employeeid);
            salary.setEmployee(emp);
            
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            salary.setPayDay(sdf.parse(rs.getString("salary_pay_day")));
            
            salary.setOtUnit(rs.getInt("salary_unit_ot"));
            salary.setOtTotal(rs.getInt("salary_ot"));
            salary.setSalary(rs.getInt("salary_balance"));
            salary.setTotal(rs.getInt("salary_total"));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        return salary;
    }
}
