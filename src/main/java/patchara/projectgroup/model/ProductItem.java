/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import patchara.projectgroup.dao.ProductDao;

/**
 *
 * @author user1
 */
public class ProductItem {

    private int id;
    String name;
    double price;
    int index;
    static int count = 1;
    int qty;
    Product product;
    Receipt receipt;

    public ProductItem(int id, int index, int qty, Product product, Receipt receipt) {
        this.id = id;
        this.index = index;
        this.qty = qty;
        this.product = product;
        this.receipt = receipt;
    }

    public ProductItem(Product product, int qty) {
        this.id = -1;
        this.product = product;
        this.qty = qty;
        this.index = count++;
    }

    public ProductItem() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        ProductItem.count = count;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    

    public static ProductItem fromRS(ResultSet rs) {
        ProductItem pdItem = new ProductItem();
        ProductDao pdDao = new ProductDao();
        try {
            pdItem.setId(rs.getInt("product_item_id"));
            int productId = rs.getInt("product_id");            
            pdItem.setName(pdDao.getName(productId));
            pdItem.setPrice(pdDao.getPrice(productId));
            pdItem.setQty(rs.getInt("product_item_qty"));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return pdItem;
    }
}
