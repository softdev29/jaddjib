package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Store {

    int id;
    String name;
    String address;
    String tel;

    public Store(int id, String name, String address, String tel) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.tel = tel;
    }

    public Store(String name, String address, String tel) {
        this.id = 1;
        this.name = name;
        this.address = address;
        this.tel = tel;
    }

    public Store() {
        this.id = 1;
    }

    public int getId() {
        return 1;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Store{" + "id=" + id + ", name=" + name + ", address=" + address + ", tel=" + tel + '}';
    }

    public static Store fromRS(ResultSet rs) {
        Store store = new Store();
        try {
            store.setId(rs.getInt("store_id"));
            store.setName(rs.getString("store_name"));
            store.setAddress(rs.getString("store_address"));
            store.setTel(rs.getString("store_tel"));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return store;
    }
}
