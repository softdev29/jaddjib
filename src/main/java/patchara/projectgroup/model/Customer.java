/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author AdMiN
 */
public class Customer {

    int id;
    private String name;
    private String telephone;
    private Date birthday;
    private int point;

    public Customer(int id, String name, String telephone, Date birthday, int point) {
        this.id = id;
        this.name = name;
        this.telephone = telephone;
        this.birthday = birthday;
        this.point = point;
    }

    public Customer() {
        this.id = -1;
    }

    public Customer(String name, String telephone, Date birthday, int point) {
        this.id = id;
        this.name = name;
        this.telephone = telephone;
        this.birthday = birthday;
        this.point = point;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Customer{" + "id=" + id + ", name=" + name + ", telephone=" + telephone + ", birthday=" + birthday + ", point=" + point + '}';
    }

    public static Customer fromRS(ResultSet rs) {
        Customer customer = new Customer();
        try {
            customer.setId(rs.getInt("customer_id"));
            customer.setName(rs.getString("customer_name"));
            customer.setTelephone(rs.getString("customer_tel"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            customer.setBirthday(sdf.parse(rs.getString("customer_birthday")));
            customer.setPoint(rs.getInt("customer_point"));
        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
            return null;
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        return customer;
    }
}
