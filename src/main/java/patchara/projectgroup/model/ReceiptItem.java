/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import patchara.projectgroup.dao.ProductDao;

/**
 *
 * @author domem
 */
public class ReceiptItem {

    private int id;
    Product product;
    private String name;
    private int qty;
    private double product_price;
    private double total;
    private Receipt receipt;
    int index;
    static int count = 1;

    public ReceiptItem(int id, Product product, String name, int qty, double product_price, double total, Receipt receipt) {
        this.id = id;
        this.product = product;
        this.name = name;
        this.qty = qty;
        this.product_price = product_price;
        this.total = total;
        this.receipt = receipt;
    }

    public ReceiptItem(Product product, String name, int qty, double product_price, double total, Receipt receipt) {
        this.id = -1;
        this.product = product;
        this.name = name;
        this.qty = qty;
        this.product_price = product_price;
        this.total = total;
        this.receipt = receipt;
    }

    public ReceiptItem(Product product, int qty, double total) {
        this.product = product;
        this.qty = qty;
        this.total = 0;
        this.index = count++;
    }

    public ReceiptItem() {
        this.id = -1;
    }

    public ReceiptItem(Product product, int qty) {
        this.product = product;
        this.qty = qty;
        this.index = count++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getProduct_price() {
        return product_price;
    }

    public void setProduct_price(double product_price) {
        this.product_price = product_price;
    }

    public double getTotal() {
        return qty * product_price;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        ReceiptItem.count = count;
    }

    @Override
    public String toString() {
        return "ReceiptItem{" + "id=" + id + ", product_id=" + product.getId() + ", name=" + name + ", qty=" + qty + ", product_price=" + product_price + ", total=" + total + '}';
    }



    public static ReceiptItem fromRS(ResultSet rs) {
        ProductDao productDao = new ProductDao();
        ReceiptItem receiptItem = new ReceiptItem();
        try {
            receiptItem.setId(rs.getInt("receipt_item_id"));
            int productId = rs.getInt("product_id");
            Product item = productDao.get(productId);
            receiptItem.setProduct(item);
            receiptItem.setName(rs.getString("name"));
            receiptItem.setQty(rs.getInt("qty"));
            receiptItem.setProduct_price(rs.getDouble("product_price"));
            receiptItem.setTotal(rs.getDouble("total"));
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return receiptItem;
    }
}
