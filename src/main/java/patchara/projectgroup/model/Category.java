
package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Category {

    private int id;
    private String name;

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category(String name) {
        this.id = -1;
        this.name = name;
    }

    public Category() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Category{" + "id=" + id + ", name=" + name + '}';
    }
    
    
    public static Category fromRS(ResultSet rs) {
        Category cat = new Category();
        try {
            cat.setId(rs.getInt("category_id"));
            cat.setName(rs.getString("category_name"));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return cat;
    }
}
