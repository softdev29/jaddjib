/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author informatics
 */
public class Stock {

    private int id;
    private String name;
    private int quantity;
    private int min;
    private Date exp;

    public Stock(int id, String name, int quantity, int min, Date exp) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.min = min;
        this.exp = exp;
    }

    public Stock() {
        this.id = -1;
    }

    public Stock(String name, int quantity, int min, Date exp) {
        this.id = -1;
        this.name = name;
        this.quantity = quantity;
        this.min = min;
        this.exp = exp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public Date getExp() {
        return exp;
    }

    public void setExp(Date exp) {
        this.exp = exp;
    }

    @Override
    public String toString() {
        return "Stock{" + "id=" + id + ", name=" + name + ", quantity=" + quantity + ", min=" + min + ", exp=" + exp + '}';
    }

    public static Stock fromRS(ResultSet rs) {
        Stock stock = new Stock();
        try {
            stock.setId(rs.getInt("stock_id"));
            stock.setName(rs.getString("stock_name"));
            stock.setQuantity(rs.getInt("stock_quantity"));
            stock.setMin(rs.getInt("stock_min"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stock.setExp(sdf.parse(rs.getString("stock_exp")));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return stock;
    }
}
