package patchara.projectgroup.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import patchara.projectgroup.dao.StoreDao;

/**
 *
 * @author L4ZY
 */
public class Employee {

    int id;
    private String name;
    private String idcard;
    private String telePhone;
    private String email;
    private Date birthday;
    private String position;
    private String address;
    private String gender;
    Store store;

    public Employee(int id, String name, String idcard, String telePhone, String email, Date birthday, String position, String address, String gender, Store store) {
        this.id = id;
        this.name = name;
        this.idcard = idcard;
        this.telePhone = telePhone;
        this.email = email;
        this.birthday = birthday;
        this.position = position;
        this.address = address;
        this.gender = gender;
        this.store = store;

    }

    public Employee(String name, String idcard, String telePhone, String email, Date birthday, String position, String address, String gender, Store store) {
        this.id = -1;
        this.name = name;
        this.idcard = idcard;
        this.telePhone = telePhone;
        this.email = email;
        this.birthday = birthday;
        this.position = position;
        this.address = address;
        this.gender = gender;
        this.store = store;

    }

    public Employee() {
        this.id = -1;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getTelePhone() {
        return telePhone;
    }

    public void setTelePhone(String telePhone) {
        this.telePhone = telePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", idcard=" + idcard + ", telePhone=" + telePhone + ", email=" + email + ", birthday=" + birthday + ", position=" + position + ", address=" + address + ", gender=" + gender + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        StoreDao storeDao = new StoreDao();
        try {
            employee.setId(rs.getInt("employee_id"));
            employee.setName(rs.getString("employee_name"));
            employee.setIdcard(rs.getString("employee_id_card"));
            employee.setTelePhone(rs.getString("employee_tel"));
            employee.setEmail(rs.getString("employee_email"));
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            employee.setBirthday(sdf.parse(rs.getString("employee_birthday")));
            employee.setPosition(rs.getString("employee_position"));
            employee.setAddress(rs.getString("employee_address"));
            employee.setGender(rs.getString("employee_gender"));
            int storeid = rs.getInt("store_id");
            Store store = storeDao.get(storeid);
            employee.setStore(store);
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
        return employee;
    }

}
