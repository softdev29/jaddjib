/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import patchara.projectgroup.helper.DatabaseHelper;
import patchara.projectgroup.model.InOut;

/**
 *
 * @author user1
 */
public class InOutDao implements Dao<InOut>{

    @Override
    public InOut get(int id) {
        InOut item = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM in_out WHERE io_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                item = InOut.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<InOut> getAll() {
        ArrayList<InOut> list = new ArrayList();
        String sql = "SELECT * FROM in_out";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                InOut item = InOut.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public InOut save(InOut obj) {
        return obj;
    }

    @Override
    public InOut update(InOut obj) {
        return obj;
    }

    @Override
    public int delete(InOut obj) {
        return -1;
    }
    
}
