/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import patchara.projectgroup.helper.DatabaseHelper;
import patchara.projectgroup.model.ProductItem;

/**
 *
 * @author user1
 */
public class ProductItemDao implements Dao<ProductItem> {

    @Override
    public ProductItem get(int id) {
        ProductItem item = null;
        String sql = "SELECT * FROM product_item WHERE product_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = ProductItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<ProductItem> getAll() {
        ArrayList<ProductItem> list = new ArrayList();
        String sql = "SELECT * FROM product_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ProductItem item = ProductItem.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ProductItem save(ProductItem obj) {
        String sql = "INSERT INTO product_item (product_item_id, product_item_name, product_item_qty, product_item_price)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
//            stmt.setString(2, obj.getSize());
//            stmt.setString(3, obj.getSweetLevel());
//            stmt.setString(4, obj.getType());
//            stmt.setInt(5, obj.getAmount());
//            stmt.setDouble(6, obj.getPrice());
//            stmt.setDouble(7, obj.getTotal());
//            stmt.setInt(8, obj.getReceipt().getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
//            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ProductItem update(ProductItem obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿UPDATE product_item"
                + " ﻿SET product_item_name = ?, product_item_qty = ?, product_item_price = ?"
                + " ﻿WHERE product_item_id = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, obj.getLogin());
//            stmt.setString(2, obj.getName());
//            stmt.setString(3, obj.getPassword());
//            stmt.setInt(4, obj.getRole());
//            stmt.setInt(5, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ProductItem obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "DELETE FROM user WHERE product_item_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setInt(1, obj.());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
