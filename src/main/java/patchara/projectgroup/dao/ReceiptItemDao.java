/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package patchara.projectgroup.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import patchara.projectgroup.helper.DatabaseHelper;
import patchara.projectgroup.model.Receipt;
import patchara.projectgroup.model.ReceiptItem;

/**
 *
 * @author domem
 */
public class ReceiptItemDao implements Dao<ReceiptItem> {

    @Override
    public ReceiptItem get(int id) {
        ReceiptItem item = null;
        String sql = "SELECT * FROM receipt_item WHERE receipt_item_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = ReceiptItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<ReceiptItem> getAll() {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReceiptItem> getByOrdersId(int receiptId) {
        return getAll("receipt_id = " + receiptId, " receipt_item_id ASC ");
    }

    public List<ReceiptItem> getAll(String where, String order) {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReceiptItem> getAll(String order) {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptItem save(ReceiptItem obj) {
        String sql = "INSERT INTO receipt_item (product_id, name, qty, product_price, total, receipt_id)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProduct().getId());
            stmt.setString(2, obj.getName());
            stmt.setDouble(3, obj.getQty());
            stmt.setDouble(4, obj.getProduct_price());
            stmt.setDouble(5, obj.getTotal());
            stmt.setInt(6, obj.getReceipt().getId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (NullPointerException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public ReceiptItem update(ReceiptItem obj) {
        return obj;
    }

    @Override
    public int delete(ReceiptItem obj) {
        String sql = "DELETE FROM receipt_item WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
    
        public int delete(int id) {
        String sql = "DELETE FROM receipt_item WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
