/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package patchara.projectgroup.dao;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import patchara.projectgroup.helper.DatabaseHelper;
import patchara.projectgroup.model.Stock;

/**
 *
 * @author informatics
 */
public class StockDao implements Dao<Stock>{

    @Override
    public Stock get(int id) {
           Stock stock = null;
        String sql = "SELECT * FROM stock WHERE stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                stock = Stock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return stock;
    }

    @Override
    public List<Stock> getAll() {
        ArrayList<Stock> list = new ArrayList();
        String sql = "SELECT * FROM stock";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Stock stock = Stock.fromRS(rs);
                list.add(stock);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return list;
    }

    @Override
    public Stock save(Stock obj) {
        String sql = "INSERT INTO stock (stock_name, stock_quantity, stock_min, stock_exp)"
                + "VALUES(?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQuantity());
            stmt.setInt(3, obj.getMin());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(4, sdf.format(obj.getExp()));

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Stock update(Stock obj) {
        String sql = "UPDATE stock"
                + " SET stock_name = ?, stock_quantity = ?, stock_min = ?, stock_exp = ?"
                + " WHERE stock_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQuantity());
            stmt.setInt(3, obj.getMin());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(4, sdf.format(obj.getExp()));
            stmt.setInt(5, obj.getId());

            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Stock obj) {
         String sql = "DELETE FROM stock WHERE stock_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

  }

