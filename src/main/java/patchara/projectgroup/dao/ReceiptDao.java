/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import patchara.projectgroup.helper.DatabaseHelper;
import patchara.projectgroup.model.Receipt;
import patchara.projectgroup.model.ReceiptItem;
import patchara.projectgroup.service.CustomerService;
import patchara.projectgroup.service.EmployeeService;
import patchara.projectgroup.service.UserService;

/**
 *
 * @author Pla
 */
public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt get(int id) {
        Receipt item = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM ﻿receipt WHERE receipt_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Receipt.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿SELECT * FROM receipt";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Receipt item = Receipt.fromRS(rs);
                list.add(item);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {
        String sql = "INSERT INTO ﻿receipt (qty, total, discount, net_total, cash, change, date, store_id, employee_id, customer_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        ReceiptItemDao recItemDao = new ReceiptItemDao();
        Connection conn = DatabaseHelper.getConnect();
        try {
            int empId = EmployeeService.getIdByName(UserService.getUserName());
            int cusId = CustomerService.getId();
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getQty());
            stmt.setDouble(2, obj.getTotal());
            stmt.setDouble(3, obj.getDiscount());
            stmt.setDouble(4, obj.getNetTotal());
            stmt.setDouble(5, obj.getCash());
            stmt.setDouble(6, obj.getChange());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(7, sdf.format(obj.getDate()));
            stmt.setInt(8, 1);
            stmt.setInt(9, empId);
            stmt.setInt(10, cusId);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
            for (ReceiptItem od : obj.getReceiptItem()) {
                recItemDao.save(od);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Receipt update(Receipt obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿UPDATE receipt"
                + " ﻿SET store_id =?, employee_id =?, customer_id =?, qty =?, total =?, discount=?, net_total=?, cash=?, change=?, date=?"
                + " ﻿WHERE receipt_id =?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1, obj.getStore().getId());
            stmt.setInt(2, obj.getEmployee().getId());
            if(obj.getCustomer()==null){
                stmt.setInt(3, 0);
            }else{
                stmt.setInt(3, obj.getCustomer().getId());
            }
            stmt.setInt(4, obj.getQty());
            stmt.setDouble(5, obj.getTotal());
            stmt.setDouble(6, obj.getDiscount());
            stmt.setDouble(7, obj.getNetTotal());
            stmt.setDouble(8, obj.getCash());
            stmt.setDouble(9, obj.getChange());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(10, sdf.format(obj.getDate()));
            stmt.setInt(11, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Receipt obj) {
        ReceiptItemDao recItemDao = new ReceiptItemDao();
        String sql = "DELETE FROM receipt WHERE receipt_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            int id = obj.getId();
            stmt.setInt(1, obj.getId());
            recItemDao.delete(id);
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
