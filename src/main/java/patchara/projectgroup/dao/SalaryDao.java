/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import patchara.projectgroup.helper.DatabaseHelper;
import patchara.projectgroup.model.Salary;

/**
 *
 * @author user1
 */
public class SalaryDao implements Dao<Salary> {

    @Override
    public Salary get(int id) {
        return null;
    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿SELECT * FROM salary";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Salary item = Salary.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Salary save(Salary obj) {
        return obj;
    }

    @Override
    public Salary update(Salary obj) {
        return obj;
    }

    @Override
    public int delete(Salary obj) {
        return -1;
    }

}
