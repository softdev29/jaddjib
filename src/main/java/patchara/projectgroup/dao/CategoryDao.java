package patchara.projectgroup.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import patchara.projectgroup.helper.DatabaseHelper;
import patchara.projectgroup.model.Category;

public class CategoryDao implements Dao<Category> {

    @Override
    public Category get(int id) {
        Category cat = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM category WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                cat = Category.fromRS(rs);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return cat;
    }

    @Override
    public List<Category> getAll() {
        ArrayList<Category> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM category";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Category cat = Category.fromRS(rs);
                list.add(cat);
            }
            stmt.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Category save(Category obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿INSERT INTO category (category_id, category_name)"
                + "VALUES(?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            stmt.setString(2, obj.getName());
            stmt.executeUpdate();
            obj.setId(DatabaseHelper.getInsertedId(stmt));
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public Category update(Category obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "﻿UPDATE category"
                + " ﻿SET category_name = ?"
                + " ﻿WHERE category_id = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Category obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "DELETE FROM category WHERE category_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
