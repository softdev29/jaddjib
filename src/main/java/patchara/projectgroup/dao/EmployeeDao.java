package patchara.projectgroup.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import patchara.projectgroup.helper.DatabaseHelper;
import patchara.projectgroup.model.Employee;

/**
 *
 * @author L4ZY
 */
public class EmployeeDao implements Dao<Employee> {

    @Override
    public Employee get(int id) {
        Employee employee = null;
        String sql = "SELECT * FROM employee WHERE employee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                employee = Employee.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return employee;
    }
    
    public int getId(String name) {
        Employee employee = null;
        String sql = "SELECT * FROM employee WHERE employee_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                employee = Employee.fromRS(rs);
            }
            if (employee == null) {
                return 0;
            }
            return employee.getId();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return 0;
        }
    }

    @Override
    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM employee";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee employee = Employee.fromRS(rs);
                list.add(employee); 
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Employee save(Employee obj) {
        String sql = "INSERT INTO employee (employee_name, employee_id_card, employee_tel, employee_email, employee_birthday, employee_position, employee_address, employee_gender, store_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getIdcard());
            stmt.setString(3, obj.getTelePhone());
            stmt.setString(4, obj.getEmail());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(5, sdf.format(obj.getBirthday()));
            stmt.setString(6, obj.getPosition());
            stmt.setString(7, obj.getAddress());
            stmt.setString(8, obj.getGender());
            stmt.setInt(9, 1);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public Employee update(Employee obj) {
        String sql = "UPDATE employee" 
                + " SET employee_name = ? , employee_id_card = ?, employee_tel = ?,employee_email = ?, employee_birthday = ?, employee_position = ?, employee_address= ?, employee_gender = ?, store_id = ?"
                +" WHERE employee_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getIdcard());
            stmt.setString(3, obj.getTelePhone());
            stmt.setString(4, obj.getEmail());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmt.setString(5, sdf.format(obj.getBirthday()));
            stmt.setString(6, obj.getPosition());
            stmt.setString(7, obj.getAddress());
            stmt.setString(8, obj.getGender());
            stmt.setInt(9, 1);
            stmt.setInt(10, obj.getId());
            stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        
    }

    @Override
    public int delete(Employee obj) {
        String sql = "DELETE FROM employee WHERE employee_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
