/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.ui;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import patchara.projectgroup.dao.InOutDao;
import patchara.projectgroup.model.InOut;

/**
 *
 * @author user1
 */
public class EmployeeWorkTime extends javax.swing.JPanel {

    private final List<InOut> list;
    InOutDao ioDao = new InOutDao();

    /**
     * Creates new form EmployeeWorkTime
     */
    public EmployeeWorkTime() {
        initComponents();
        list = ioDao.getAll();
        tblWorkTime.setModel(new AbstractTableModel() {
            
            String[] columnNames = {"ID", "Employe ID", "Date", "Start Time", "End Time", "Period"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }
            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                InOut io = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return io.getId();
                    case 1:
                        return io.getEmployee().getId();
                    case 2:
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        return sdf.format(io.getDate());
                    case 3:
                        return io.getStartTime();
                    case 4:
                        return io.getEndTime();
                    case 5:
                        return io.getPeriod();
                    default:
                        return "";
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblWorkTime = new javax.swing.JTable();

        setBackground(new java.awt.Color(191, 124, 75));
        setMinimumSize(new java.awt.Dimension(848, 578));

        tblWorkTime.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tblWorkTime.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblWorkTime);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 836, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 566, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblWorkTime;
    // End of variables declaration//GEN-END:variables
}
