/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package patchara.projectgroup.service;

import java.util.List;
import patchara.projectgroup.dao.StockDao;
import patchara.projectgroup.model.Stock;

/**
 *
 * @author iamba
 */
public class StockService {

    public List<Stock> getAll() {
        StockDao stockDao = new StockDao();
        return stockDao.getAll();
    }

    public Stock addNew(Stock editedStock) {
        StockDao stockDao = new StockDao();
        return stockDao.save(editedStock);
    }

    public Stock update(Stock editedStock) {
        StockDao stockDao = new StockDao();
        return stockDao.update(editedStock);
    }

    public int delete(Stock editedStock) {
        StockDao stockDao = new StockDao();
        return stockDao.delete(editedStock);
    }

}
