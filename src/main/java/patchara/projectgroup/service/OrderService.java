/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package patchara.projectgroup.service;

import java.util.List;
import patchara.projectgroup.dao.ReceiptDao;
import patchara.projectgroup.model.Receipt;

/**
 *
 * @author domem
 */
public class OrderService {

    public List<Receipt> getOrder() {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll();
    }

    public List<Receipt> getAll() {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.getAll();

    }

    public int delete(Receipt editedOrder) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.delete(editedOrder);
    }

    public Receipt update(Receipt editedOrder) {
        ReceiptDao receiptDao = new ReceiptDao();
        return receiptDao.update(editedOrder);
    }
}
