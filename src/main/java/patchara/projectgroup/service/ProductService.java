/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.service;

import java.util.ArrayList;
import java.util.List;
import patchara.projectgroup.dao.ProductDao;
import patchara.projectgroup.model.Product;

/**
 *
 * @author user1
 */
public class ProductService {

    public static List<Product> getAllProduct() {
        ProductDao productDao = new ProductDao();
        return productDao.getAll();
    }

    public static ArrayList<Product> getMockProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list = (ArrayList<Product>) ProductService.getAllProduct();
        return list;
    }
}
