package patchara.projectgroup.service;

import java.util.List;
import patchara.projectgroup.dao.UserDao;
import patchara.projectgroup.model.User;

public class UserService {

    static String name;

    public static User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getLogin(login);
        if (user != null && user.getPassword().equals(password)) {
            name = user.getName();
            return user;
        }
        return null;
    }
    
    public static String getUserName(){
        return name;
    }
    
    public static List<User> getUsers() {
        UserDao userDao = new UserDao();
        return userDao.getAll();
    }

    public User updateUser(User user) {
        UserDao userDao = new UserDao();
        return userDao.update(user);
    }

    public User newUser(User user) {
        UserDao userDao = new UserDao();
        return userDao.save(user);
    }

    public int deleteUser(User user) {
        UserDao userDao = new UserDao();
        return userDao.delete(user);
    }
}
