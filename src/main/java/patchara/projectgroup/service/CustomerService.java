/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.service;

import java.util.List;
import patchara.projectgroup.dao.CustomerDao;
import patchara.projectgroup.model.Customer;

/**
 *
 * @author user1
 */
public class CustomerService {

    static int cusId;

    public static List<Customer> getAllCustomer() {
        CustomerDao cusDao = new CustomerDao();
        List<Customer> cus = cusDao.getAll();
        return cus;
    }

    public static String getCusName(String tel) { // get cus name by tel
        CustomerDao cusDao = new CustomerDao();
        String cus = cusDao.getByTel(tel);
        if (cus == null) {
            return null;
        }
        return cus;
    }

    public static int getCusPoint(String tel) { // from tel get cus point
        CustomerDao cusDao = new CustomerDao();
        String cus = cusDao.getByTel(tel);
        if (cus == null) {
            return 0;
        }
        return cusDao.getByName(cus).getPoint();
    }

    public static List<String> getAllCusTel() { // get all cus tel
        CustomerDao cusDao = new CustomerDao();
        List<Customer> cus = cusDao.getAll();
        List<String> tel = null;
        for (Customer s : cus) {
            tel.add(s.getTelephone());
        }
        return tel;
    }

    public static void setId(String name) { // set id by name
        CustomerDao cusDao = new CustomerDao();
        cusId = cusDao.getIdByName(name);
    }

    public static int getId() {
        return cusId;
    }

    public static void updatePoint(int additionPoint) {
        CustomerDao cusDao = new CustomerDao();
        Customer cus = cusDao.get(cusId);
        cus.setPoint(additionPoint);
        cusDao.update(cus);
    }

    public static void resetId() {
        cusId = 0;
    }

    public static Customer addNew(Customer editedCustomer) {
          CustomerDao customerDao = new CustomerDao();
          return customerDao.save(editedCustomer);
    }

    public Customer update(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.update(editedCustomer);
    }
    
    public int delete(Customer editedCustomer) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.delete(editedCustomer);
    }
}
