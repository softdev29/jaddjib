/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patchara.projectgroup.service;

import java.util.List;
import patchara.projectgroup.dao.EmployeeDao;
import patchara.projectgroup.model.Employee;

/**
 *
 * @author user1
 */
public class EmployeeService {

    public static int getIdByName(String name) {
        EmployeeDao empDao = new EmployeeDao();
        int empId = empDao.getId(name);
        return empId;
    }

    public List<Employee> getAll() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll();
    }

    public Employee addNew(Employee edtEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.save(edtEmployee);
    }

    public Employee update(Employee edtEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.update(edtEmployee);
    }

    public int delete(Employee edtEmployee) {
        EmployeeDao empDao = new EmployeeDao();
        return empDao.delete(edtEmployee);
    }
}
